# My Personal Configuration Files

## Liability
*__TLDR:__ Not my problem mate!*

Just my various configuration files. Take what you like, all of it or what you don't like, I don't care. If you get fired, make your computer smoke, cause the apocalypse or make a billion trillion currency I genuinely don't care. That's your problem have fun with it (alright if you end the world I will be a bit salty).

## Programs I Use
*List of programs whose configuration files are stored here. I use way more that don't have files.*

### Text Editors
* [Vim](https://www.vim.org/)
* [Vundle](https://github.com/VundleVim/Vundle.vim)
* [Atom](https://atom.io/)

### Terminal
* [Fish](https://fishshell.com/)
* [URxvt](http://software.schmorp.de/pkg/rxvt-unicode.html)

### IDEs
* [Android Studio](https://developer.android.com/studio/)
* [IntelliJ/idea](https://www.jetbrains.com/idea/)
* [PyCharm](https://www.jetbrains.com/pycharm/?fromMenu)
* [GoLand](https://www.jetbrains.com/go/?fromMenu)

### File Management
* [Ranger](https://ranger.github.io/)
* [Git](https://git-scm.com/)
* [Rsync](https://rsync.samba.org/)

### Window Manager
* [i3](https://i3wm.org/)
* [i3-gaps](https://github.com/Airblader/i3)

### Package Management
* [Pacman](https://www.archlinux.org/pacman/)
* [Makepkg](https://wiki.archlinux.org/index.php/Makepkg)
* [Yay](https://github.com/Jguer/yay)

### Theming
* [Xresources](https://wiki.archlinux.org/index.php/X_resources)

### Media
* [Jack](https://jackaudio.org/)
* [Cadence](https://kx.studio/Applications:Cadence)
* [Carla](https://kx.studio/Applications:Carla)
* [Mpv](https://mpv.io/)
* [Youtube-dl](https://youtube-dl.org/)

## Fonts
See the [fonts folders README.md file](Fonts/README.md) for more information on what fonts I use, where I use them and how to get them.

## Structure
Folder for each program. Scripts folder for management scripts. Pretty simple. README files o' plenty with info about themes, plugins, etc.

## Contact
*If you find any problems with the files or configurations described in this repository feel free to send me an email. If you have questions feel free to do the same.*

* __Email:__ `public@patrickhoette.email`
* __LinkedIn:__ [patrickhoette](https://www.linkedin.com/in/patrickhoette/)

