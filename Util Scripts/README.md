# Utility Scripts
*This folder contains a collection of small scripts to help automate config file management*

__DISCLAIMER:__ I am by no means a bash programmer. I am at most a starting hobbiest in the language. Being used to more modern languages (I think the oldest I have worked with for a good bit of time is Java 6) BASH is quite foreign to me. Please make sure that these scripts will work for you before running them. I hope I didn't break to many conventions and best practises. If you see a problem with any script or any other part of my setup feel free to send an email to `public@patrickhoette.email`.
